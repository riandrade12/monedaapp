FROM openjdk:8
VOLUME /tmp
EXPOSE 8092
ADD ./target/user-service-0.0.1-SNAPSHOT.jar service.jar
ENTRYPOINT ["java","-jar","/service.jar"]