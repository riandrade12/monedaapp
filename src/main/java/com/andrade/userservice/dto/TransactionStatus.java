package com.andrade.userservice.dto;

public enum TransactionStatus {
	APPROVED,
	DECLINED;

}
