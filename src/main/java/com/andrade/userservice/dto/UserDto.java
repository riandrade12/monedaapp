package com.andrade.userservice.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UserDto {

	private Integer id;
	private String origen;
	private String destino;
	private Integer monto;

	
	
}
