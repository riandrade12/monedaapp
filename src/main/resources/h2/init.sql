create table users (
   id bigint auto_increment,
   origen varchar(50),
   destino varchar(50), 
   monto int,
   primary key (id)
  );
  
  create table user_transaction(
     id bigint auto_increment,
     user_id bigint,
     amount int,
     transaction_date timestamp,
     foreign key (user_id) references users(id) on delete cascade
  );